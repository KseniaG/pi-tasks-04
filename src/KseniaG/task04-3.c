#include<stdio.h>
#include<string.h>
#define N 10
#define M 256
int main()
{
	int i = 0, j = 0;
	char str[N][M];
	printf("Enter a lines\n");
	for (i = 0; i < N; i++)
	{
		gets(str[i]);
		if (str[i][0] == '\0')
		{
			break;
		}
	}
	printf("Result\n\n");
	for (j = 1; j <= M; j++)
	{
		for (i = 0; i < N; i++)
		{
			if (strlen(str[i]) == j)
				printf("%s\n", str[i]);
		}
	}
	return 0;
}