﻿#include<stdio.h>
#include<string.h>
#define N 10
#define M 256
int main()
{
	int i = 0, j = 0, t;
	int arr[N];
	char str[N][M] = { 0 };
	srand(time(0));
	printf("Enter a lines\n");
	for (i = 0; i < N; i++)
	{
		gets(str[i]);
		if (str[i][0] == '\0')
		{
			break;
		}
	}
	printf("Result\n\n");
	for (i = 0; i < N + 1; i++)
	{
		arr[i] = rand() % 10;
		for (j = 0; j < i; j++)
		{
			if (arr[i] == arr[j])
			{
				t = 0;
				i--;
				break;
			}
			else
				t = 1;
		}
		if (t)
			printf("%s\n", str[arr[i]]);
	}
	return 0;
}